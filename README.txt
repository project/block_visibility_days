CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------
This module Enhances visibility settings by adding days condition.


INSTALLATION
------------
Enable the module.

CONFIGURATION
------------
To add days to a block, simply visit that block's configuration page visibility settings at
Administration > Structure > Blocks

/**
 * @file
 * Settings restricted values on date fieldset.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.blockVisibilityDaysSettings = {
    attach: function () {
      function checkboxesSummary(context) {
        const values = [];
        const $checkboxes = $(context).find(
          'input[type="checkbox"]:checked + label',
        );
        const il = $checkboxes.length;
        for (let i = 0; i < il; i++) {
          values.push($($checkboxes[i]).html());
        }
        if (!values.length) {
          values.push(Drupal.t('Not restricted'));
        }
        return values.join(', ');
      }

      $('[data-drupal-selector="edit-visibility-block-visibility-days"]').drupalSetSummary(checkboxesSummary);
    }
  }
})(jQuery);

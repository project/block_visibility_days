<?php

namespace Drupal\block_visibility_days\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'days' condition.
 *
 * @Condition(
 *   id = "block_visibility_days",
 *   label = @Translation("Days Visibility"),
 * )
 */
class BlockVisibilityDays extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Adding days fieldset to the block configure page.
    $form['days_visibility'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Days Visibility'),
    ];

    $form['days_visibility']['days'] = [
      '#type' => 'checkboxes',
      '#options' => ['Sun'=>'Sunday','Mon'=>'Monday','Tue'=>'Tuesday','Wed'=>'Wednesday','Thu'=>'Thursday','Fri'=>'Friday','Sat'=>'Saturday'],
      '#title' => $this->t('Days'),
      '#default_value' => $this->configuration['days_visibility']['days'],
      '#description' => $this->t('Shows the block on pages matching days.'),
    ];

    $form['#attached']['library'][] = 'block_visibility_days/block_visibility_days';
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'days_visibility' => [
        'days' => '',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['days_visibility'] = $form_state->getValues()['days_visibility'];
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {}

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $days = $this->configuration['days_visibility'];
    $days_visibility = (array_filter($days['days']));
    if(empty($days_visibility)) {
      return TRUE;
    }
    // Get the current Unix day.
    $today = date('D');
    if (in_array($today, $days_visibility)) {
      return TRUE;
    }
    return FALSE;
  }

}
